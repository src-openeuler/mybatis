%bcond_with test
Name:                mybatis
Version:             3.5.10
Release:             2
Summary:             SQL Mapping Framework for Java
License:             Apache 2.0
URL:                 https://github.com/mybatis/mybatis-3
Source0:             https://github.com/mybatis/mybatis-3/archive/%{name}-%{version}.tar.gz
BuildRequires:       mvn(net.java.dev.jna:jna)
BuildRequires:       mvn(org.apache.maven.wagon:wagon-ssh-common)
BuildRequires:       mvn(org.apache.maven.plugins:maven-site-plugin)
BuildRequires:       mvn(org.apache.logging.log4j:log4j-core)
BuildRequires:       mvn(ch.qos.logback:logback-classic)
BuildRequires:       mvn(org.apache.derby:derby)
BuildRequires:       mvn(net.bytebuddy:byte-buddy)
BuildRequires:       mvn(com.fasterxml.jackson.core:jackson-annotations)
BuildRequires:       maven maven-local java-1.8.0-openjdk-devel
BuildRequires:       mvn(org.eclipse.aether:aether-connector-basic)
BuildRequires:       mvn(org.eclipse.aether:aether-transport-wagon)
BuildRequires:       mvn(org.apache.maven.wagon:wagon-http)
BuildRequires:       mvn(org.apache.maven.wagon:wagon-provider-api)
Requires:            java-1.8.0-openjdk javapackages-tools
BuildArch:           noarch

%description
The MyBatis data mapper framework makes it easier
to use a relational database with object-oriented
applications. MyBatis couples objects with stored
procedures or SQL statements using a XML descriptor
or annotations. Simplicity is the biggest advantage
of the MyBatis data mapper over object relational
mapping tools.
To use the MyBatis data mapper, you rely on your
own objects, XML, and SQL. There is little to
learn that you don't already know. With the
MyBatis data mapper, you have the full power of
both SQL and stored procedures at your fingertips.
The MyBatis project is developed and maintained by
a team that includes the original creators of the
"iBATIS" data mapper. The Apache project was retired
and continued here.

%package javadoc
Summary:             Javadoc for %{name}
%description javadoc
This package contains javadoc for %{name}.

%prep
%autosetup -n %{name}-3-%{name}-%{version} -p1
%pom_remove_parent
%pom_xpath_inject "pom:project" "<groupId>org.mybatis</groupId>"
%pom_xpath_inject "pom:properties" "<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>"
%pom_xpath_inject "pom:properties" "<encoding>UTF-8</encoding>"
%pom_xpath_inject "pom:properties" "<java.version>1.8</java.version>"
%pom_xpath_inject "pom:properties" "<maven.compiler.source>1.8</maven.compiler.source>"
%pom_xpath_inject "pom:properties" "<maven.compiler.target>1.8</maven.compiler.target>"
%pom_remove_plugin :maven-pdf-plugin
%pom_remove_plugin :maven-surefire-plugin
%pom_remove_plugin :maven-shade-plugin
sed -i 's/\r//' LICENSE NOTICE
rm src/test/java/org/apache/ibatis/cache/SoftCacheTest.java
%mvn_file :%{name} %{name}

%build
%if "%{_arch}" == "riscv64"
export JAVA_TOOL_OPTIONS="-Xmx4096m"
%endif
%mvn_build -b

%install
%mvn_install

%files -f .mfiles
%license LICENSE NOTICE

%files javadoc
%{_javadocdir}/mybatis
%license LICENSE NOTICE

%changelog
* Wed Sep 25 2024 Dingli Zhang <dingli@iscas.ac.cn> - 3.5.10-2
- Update spec for riscv64 due to OOM error

* Wed Apr 03 2024 Ge Wang <wang__ge@126.com> - 3.5.10-1
- Upgrade to version 3.5.10

* Mon Aug 22 2022 wangkai <wangkai385@h-partners.com> - 3.5.8-2
- Rebuild for log4j 2.17.2 fix CVE-2021-44832

* Tue Apr 19 2022 wangkai <wangkai385@h-partners.com> - 3.5.8-1
- Upgrade 3.5.8

* Sat Jun 19 2021 lingsheng <lingsheng@huawei.com> - 3.2.8-2
- Fix CVE-2020-26945

* Fri Jan 8 2021 chengzihan <chengzihan2@huawei.com> - 3.2.8-1
- Package init
